<?php
class sampleClass extends helper{

    var $sample1 = "";

    /***************************************
       #1 Sort the string alphabetically.
    ***************************************/

    // Accepts Arrray and transfer the lowest value to temp array
    function sample($array, $temp=""){
        if(count($array)){
            $data = $this->get_lowest($array);
            $temp[] = $data["lowest_val"];
            $this->sample1 = $temp;
            unset($array[$data["lowest_key"]]);
            $this->sample($array, $temp);
        }
    }

    // Display the result in string.
    function result(){
        $val = implode("",$this->sample1);
        return $val;
    }

    /**********************************************/
        #2 Takes a string detect it is palindrome
    /**********************************************/

    function sample2($str){

        if (strlen($str) >= 1){

            $front = substr($str,0,1);
            $back = substr($str,(strlen($str) - 1),1);

             if ($front  == $back) {
                  return $this->sample2(substr($str,1,strlen($str) -2));
             }else{
                 return false;
             }

        }else{
             return true;
        }

    }
    /******************************************
        #3 Takes an array and reverse the
           elments and convert back into str.
    /*****************************************/

    function sample3($array){
        $return = array();
        $total = count($array) - 1;
        for($i=$total; $i>=0; $i--){
            $return[] = $array[$i];
        }
        $val = $this->result3($return);
        return $val;
    }

    // Display the result in string.
    function result3($return){
        $val = implode("",$return);
        return $val;
    }

    /******************************************
        #4 Takes an array and remove all
           elements having duplicates.
    /******************************************/

    function sample4($array){

        foreach($array as $key => $value){
            foreach($array as $key2 => $value2){
                if(strcmp($value,$value2) == 0 && $key != $key2){
                    unset($array[$key2]);
                }
            }
        }
        $val = $this->result4($array);
        return $val;
    }

    // Display the result in string.
    function result4($return){
        $val = implode("",$return);
        return $val;
    }

    /*************************************/
        #5 
    /*************************************/

    function sample5($data){

        date_default_timezone_set('America/Los_Angeles');

        list($aa,$bb) = explode("-",$data['sample5a']);
        list($cc,$dd) = explode("-",$data['sample5b']);

        $a = new DateTime($aa);
        $b = new DateTime($bb);
        $c = new DateTime($cc);
        $d = new DateTime($dd);

        $interval1 = $b->diff($c);
        $interval2 = $d->diff($a);

         $raw1 = $interval1->format("%H:%i");
         $raw2 = $interval2->format("%H:%i");

         if($raw1 < $raw2){
             $return = $interval1->format("%H hours and %i minutes");
         }else{
             $return = $interval2->format("%H hours and %i minutes");
         }

         return $return;
    }

}
?>
