<?php

class  helper{

    function get_lowest($array){

        $data['lowest_val'] = "";
        $data['lowest_key'] = "";

        foreach($array as $key => $value){
            if($data['lowest_val']==""){
                $data['lowest_val'] = $value;
                $data['lowest_key'] = $key;
            }else{
                $check=strcmp($value,$data['lowest_val']);
                if($check <= 0){
                    $data['lowest_val'] = $value;
                    $data['lowest_key'] = $key;
                }
            }
        }
        return $data;
    }
}

?>
