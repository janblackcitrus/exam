<?php
    require_once("helper.php");
    require_once("class.php");
    $obj = new sampleClass;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="">
            <p>1. Write a function that sorts a string of characters in alphabetical order to return it. </p>
            <p>For examples: </p>
            <p>Input: “acbaebfdg” </p>
            <p>Output: “aabbcdefg” </p>
        </div>
        <code></code>
        <?php
            //$str = "acbaebfdg";
            $sample1 = (isset($_POST["sample1"])) ? $_POST["sample1"] : "";
            if(isset($_POST["flag-1"]) ){
                $str = $sample1;
                $array = str_split($str);

                $obj->sample($array);
                $result1 = $obj->result();
                echo '<p>Output: “'.$result1.'” </p>';
            }
        ?>
        <form action="" method="post">
            <input type="hidden" name="flag-1" value="1">
            <input type="text" name="sample1" value="<?=$sample1?>">
            <input type="submit" name="name" value="Submit">
        </form>
        <hr/>
        <div class="">
            <p>2. Using recursion (not a sequential loop) method call, write a function which takes in a string, and return a boolean value to tell whether the word is a palindrome. A palindrome is any word which reads the same when reversed. </p>
            <p>For examples:</p>
            <p>Input: "abcba", Output: true</p>
            <p>Input: "abcabc", Output: false</p>
            <p>Input: "pop", Output: true</p>
            <p>Input: "abba", Output: true</p>
        </div>
        <code></code>
        <?php
            //$str = "acbaebfdg";
            $sample2 = (isset($_POST["sample2"])) ? $_POST["sample2"] : "";
            if(isset($_POST["flag-2"]) ){
                $str = $sample2;
                $check2 = $obj->sample2($str);
                if ($check2 == true){
                    echo "Output: true";
                }else {
                    echo "Output: false";
                }
            }
        ?>
        <form action="" method="post">
            <input type="hidden" name="flag-2" value="1">
            <input type="text" name="sample2" value="<?=$sample2?>">
            <input type="submit" name="name" value="Submit">
        </form>
        <hr/>
        <div class="">
            <p>3. Write a function which takes in a string as a input and outputs a string in reverse. </p>
            <p>For examples:</p>
            <p>Input: “abcdefg”</p>
            <p>Output: “gfedcba”</p>
        </div>
        <code></code>
            <?php
                $sample3 = (isset($_POST["sample3"])) ? $_POST["sample3"] : "";
                if(isset($_POST["flag-3"]) ){

                    $str = $sample3;
                    $array = str_split($str);
                    $result3 = $obj->sample3($array);
                    echo '<p>Output: “'.$result3.'” </p>';
                }
            ?>
            <form action="" method="post">
                <input type="hidden" name="flag-3" value="1">
                <input type="text" name="sample3" value="<?=$sample3?>">
                <input type="submit" name="name" value="Submit">
            </form>

        <hr/>

        <p>4. Given an array of strings, write a function that removes any duplicate values.<p>
            <?php
                $array1 = array("a","b","a","c","a","d","b","e");
                $array2 = array("a","b","b");
                $array3 = array("b","b","b");
                $result4a = $obj->sample4($array1);
                $result4b = $obj->sample4($array2);
                $result4c = $obj->sample4($array3);

            ?>
        <p> For examples:<p>
        <p>1) Input: [ “a”, “b”, “a” , “c”, “a”, “d”, “b”, “e”],
              <br/>Output: <?=$result4a?><p>
        <p>2) Input: [“a”, “b”, “b”],
              <br/>Output: <?=$result4b?><p>
        <p>3) Input: [“b”, “b”, “b”],
              <br/>Output: <?=$result4c?><p>

        <hr/>
        <p>5. Given an array of 2 set of times. Write a function to find the shortest gap time between each set of times, and return it as hours and minutes. (You can use built in functions for this one)<p>
        <?php
            $sample5a = isset($_POST["sample5a"]) ? $_POST["sample5a"] : "12:30AM-1:30AM";
            $sample5b = isset($_POST["sample5b"]) ? $_POST["sample5b"] : "2:30AM-10:30PM";

            if(isset($_POST["flag-5"]) && $sample5a != "" && $sample5b != ""){
                $data["sample5a"] = $sample5a;
                $data["sample5b"] = $sample5b;

                $result5 = $obj->sample5($data);
                echo $result5;
            }
        ?>
        <form action="" method="post">
            <input type="hidden" name="flag-5" value="1">
            Time 1: <br/><input type="text" name="sample5a" value="<?=$sample5a?>"><br/>
            Time 2: <br/><input type="text" name="sample5b" value="<?=$sample5b?>"><br/>
            <input type="submit" name="name" value="Submit">
        </form>
    </body>
</html>
